<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=45.147.178.84;dbname=spai-site;port=5432',
    'username' => 'spai-site',
    'password' => 'spai-site',
    'charset' => 'utf8',

    // Schema cache options (for production environment)
    //'enableSchemaCache' => true,
    //'schemaCacheDuration' => 60,
    //'schemaCache' => 'cache',
];
