<?php

namespace app\commands;

use app\models\aggregated\Related_by_categories;
use app\models\aggregated\Related_by_tags;
use app\models\aggregated\Related_posts;
use app\models\external\Post_categories;
use app\models\external\Post_tags;
use app\models\external\Posts;
use app\models\Site;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class AggregatorController extends Controller
{
    public function actionGetPostsToAggregate(int $count = 10)
    {

        //не аггрегировать посты сайтов, у которых менее 90% загруженных статей
        $disable_sites = Site::find()
            ->select('id')
            ->where('synced_posts < total_posts * 0.9')
            ->column();

        $posts = Posts::find()
            ->where([
                'recommend' => true
            ])
            ->andWhere(['or',
                ['aggregated_posts' => false],
                ['aggregated_categories' => false],
                ['aggregated_tags' => false]
            ])
            ->andWhere(['NOT IN', 'site_id', $disable_sites])
            ->select('id')
            ->orderBy('id ASC')
            ->limit($count)
            ->column();

        echo Json::encode($posts);
    }

    public function actionProcessPost($post_id)
    {
        $result = [
            'post_id' => $post_id,
            'aggregated' => []
        ];

        $post = Posts::find()
        ->where(['id' =>$post_id])
        ->one();

        if($post) {
            if($post->aggregated_tags == false) {
                $tags_result = self::aggregateTags($post->id);
                $result['aggregated']['tags'] = $tags_result;

                $post->aggregated_tags = true;
            }

            if($post->aggregated_categories == false) {
                $categories_result = self::aggregateCategories($post->id);
                $result['aggregated']['categories'] = $categories_result;

                $post->aggregated_categories = true;
            }

            if($post->aggregated_posts == false) {
                $posts_result = self::aggregatePosts($post->id);
                $result['aggregated']['posts'] = $posts_result;

                $post->aggregated_posts = true;
            }

            $post->save();
        }

        echo Json::encode($result);
    }

    private function aggregateTags($post_id)
    {

        /**
         * @var array
         * id постов, которые имеют хотя бы 1 совместный тег.
         * Sample: [post_id => value]
         */
        $posts_with_tag_ids = [];

        self::deleteTagsRelationsOldValues($post_id);

        $current_post_tags = Post_tags::find()
            ->where([
                'deleted' => false,
                'post_id' => $post_id
            ])
            ->all();

        //var_dump(ArrayHelper::getColumn($current_post_tags, 'id'));

        if($current_post_tags) {
            $posts_with_tag_ids = self::getPostsWithTag($current_post_tags, $post_id);

            self::setNewTagsRelations($posts_with_tag_ids, $post_id);
        }
        return $posts_with_tag_ids;
    }

    private function aggregateCategories($post_id)
    {
        /**
         * @var array
         * id постов, которые имеют хотя бы 1 совместную категорию.
         * Sample: [post_id => value]
         */
        $posts_with_category_ids = [];

        self::deleteCategoriesRelationsOldValues($post_id);

        $current_post_categories = Post_categories::find()
            ->where([
                'deleted' => false,
                'post_id' => $post_id
            ])
            ->all();

        if($current_post_categories) {
            $posts_with_category_ids = self::getPostsWithCategory($current_post_categories, $post_id);

            self::setNewCategoriesRelations($posts_with_category_ids, $post_id);
        }
        return $posts_with_category_ids;
    }

    /**
     * Простановка новых значений
     * @param $posts_with_tag_ids
     * @param $post_id
     */
    private function setNewTagsRelations($posts_with_tag_ids, $post_id)
    {
        if(count($posts_with_tag_ids)>0) {
            foreach ($posts_with_tag_ids as $related_post_id => $related_value) {
                $related_by_tag = Related_by_tags::find()
                    ->where([
                        'post_id' => $post_id,
                        'related_post_id' => $related_post_id
                    ])
                    ->one();
                if (!$related_by_tag) {
                    $related_by_tag = new Related_by_tags();
                    $related_by_tag->post_id = $post_id;
                    $related_by_tag->related_post_id = $related_post_id;
                }
                $related_by_tag->value = $related_value;
                $related_by_tag->save();
            }
        }
    }

    /**
     * Простановка новых значений
     * @param $posts_with_category_ids
     * @param $post_id
     */
    private function setNewCategoriesRelations($posts_with_category_ids, $post_id)
    {
        if(count($posts_with_category_ids)>0) {
            foreach ($posts_with_category_ids as $related_post_id => $related_value) {
                $related_by_category = Related_by_categories::find()
                    ->where([
                        'post_id' => $post_id,
                        'related_post_id' => $related_post_id
                    ])
                    ->one();
                if (!$related_by_category) {
                    $related_by_category = new Related_by_categories();
                    $related_by_category->post_id = $post_id;
                    $related_by_category->related_post_id = $related_post_id;
                }
                $related_by_category->value = $related_value;
                $related_by_category->save();
            }
        }
    }

    /**
     * Обнуление предыдущих значений, если ранее проводилась аггрегирование
     * @param $post_id
     */
    private function deleteTagsRelationsOldValues($post_id)
    {
        Related_by_tags::updateAll(['value' => 0], ['=', 'post_id', $post_id]);
    }

    /**
     * Обнуление предыдущих значений, если ранее проводилась аггрегирование
     * @param $post_id
     */
    private function deleteCategoriesRelationsOldValues($post_id)
    {
        Related_by_categories::updateAll(['value' => 0], ['=', 'post_id', $post_id]);
    }

    private function getPostsWithTag($current_post_tags, $post_id)
    {
        $posts_with_tag_ids = [];

        foreach ($current_post_tags as $current_post_tag) {
            $posts_with_tag = Post_tags::find()
                ->where([
                    'deleted' => false,
                    'tag_id' => $current_post_tag->tag_id
                ])
                ->andWhere(['<>','post_id', $post_id])
                ->all();

            if($posts_with_tag) {
                foreach ($posts_with_tag as $post_with_tag) {
                    if(isset($posts_with_tag_ids[$post_with_tag->post_id])) {
                        $posts_with_tag_ids[$post_with_tag->post_id]++;
                    } else {
                        ArrayHelper::setValue($posts_with_tag_ids, $post_with_tag->post_id, 1);
                    }
                }
            }
        }
        return $posts_with_tag_ids;
    }

    private function getPostsWithCategory($current_post_categories, $post_id)
    {
        $posts_with_category_ids = [];
        foreach ($current_post_categories as $current_post_category) {
            $posts_with_category = Post_categories::find()
                ->where([
                    'deleted' => false,
                    'category_id' => $current_post_category->category_id
                ])
                ->andWhere(['<>','post_id', $post_id])
                ->all();

            if($posts_with_category) {
                foreach ($posts_with_category as $post_with_category) {
                    if(isset($posts_with_category_ids[$post_with_category->post_id])) {
                        $posts_with_category_ids[$post_with_category->post_id]++;
                    } else {
                        ArrayHelper::setValue($posts_with_category_ids, $post_with_category->post_id, 1);
                    }
                }
            }
        }
        return $posts_with_category_ids;
    }

    private function aggregatePosts($post_id)
    {
        $all_related_posts = [];

        self::deletePostsRelationsOldValues($post_id);

        $related_by_tags = Related_by_tags::find()
            ->where([
                'post_id' => $post_id
            ])
            ->asArray()
            ->all();

        $all_related_posts = $tags_relations = ArrayHelper::map($related_by_tags, 'related_post_id', 'value');


        $related_by_categories = Related_by_categories::find()
            ->where([
                'post_id' => $post_id
            ])
            ->asArray()
            ->all();

        $categories_relations = ArrayHelper::map($related_by_categories, 'related_post_id', 'value');

        if(count($categories_relations) > 0) {
            foreach ($categories_relations as $category_relations_key => $category_relations_value) {
                if(isset($all_related_posts[$category_relations_key])) {
                    $all_related_posts[$category_relations_key] = $all_related_posts[$category_relations_key] + $category_relations_value;
                } else {
                    ArrayHelper::setValue($all_related_posts, $category_relations_key, $category_relations_value);
                }

            }
        }

        foreach ($all_related_posts as $related_post_key => $related_post_value) {
            $related_post = Related_posts::find()
                ->where([
                    'post_id' => $post_id,
                    'related_post_id' => $related_post_key
                ])
                ->one();
            if(!$related_post) {
                $related_post = new Related_posts();
                $related_post->post_id = $post_id;
                $related_post->related_post_id = $related_post_key;
            }
            $related_post->value = $related_post_value;
            $related_post->save();
        }

        return $all_related_posts;
    }

    /**
     * Обнуление ранней агррегированной инфы
     * @param $post_id
     */
    private function deletePostsRelationsOldValues($post_id)
    {
        Related_posts::updateAll(['value' => 0], ['=', 'post_id', $post_id]);
    }
}
