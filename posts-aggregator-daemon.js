#!/usr/bin/env node
const { exec } = require('child_process');

var t = process.hrtime();

const MAX_RUNNING_UPDATES = 10;

var POSTS_QUEUE = []; // waiting for processing
var running_updates = 0;
var RUNNING_UPDATES_START_TIME = new Map();
var count_ended_processes = 0;

//const YIIC = 'php /var/www/yii2-getter.local/basic/yii';
const YIIC = 'timeout 60 php /var/www/html/yii';

var grabbing_new_posts = false;

function getRandomNumber(min, max) {
    return Math.random() * (max - min) + min;
}

function time(){
    return parseInt(new Date().getTime()/1000)
}

function yiic(command, callback)
{
    exec( YIIC + ' ' + command, callback);
}

function processPost(id)
{
    console.log('processing post #' + id);
    var start_time = time();
    RUNNING_UPDATES_START_TIME.set(id, new Date());

    yiic('aggregator/process-post ' + id, (err, stdout, stderr) => {
        if(stderr) {
            console.error(stderr);
        }
        if(stdout) {
            console.log(stdout);
        }

        processPostFinished(id, start_time);
    });

    running_updates++;
}

function processPostFinished(id, start_time)
{
    count_ended_processes++;
    var end_time = time();
    var parse_time = end_time - start_time;
    console.log('update finished #'+id+' Parse time: '+parse_time+' sec');
    console.log('Count ended processes:'+count_ended_processes);
    RUNNING_UPDATES_START_TIME.delete(id);
    running_updates--;

    setTimeout(processNewPosts, 3*100);// why wait at all ?
}

function processNewPosts()
{
    //console.log('---Process NEW posts---');
    console.log('--- ' + POSTS_QUEUE.length + ' posts in queue---');
    console.log('--- ' + running_updates + ' running updates---');

    var n = MAX_RUNNING_UPDATES - running_updates;
    for(var i = 0; i < n; i++) {
        if(POSTS_QUEUE.length) {
            var post_id = POSTS_QUEUE.pop();
            processPost(post_id);
        } else {
            console.log('POSTS_QUEUE ended.');
            grabNewPosts();
        }
    }

    if(POSTS_QUEUE.length == 0 && running_updates == 0) {
        t = process.hrtime(t);
        console.log( Math.floor(t[0] / 60) + " minutes, " + t[0] % 60 + " seconds");

        /*exec('ps -ef | grep phantomjs | awk "{print $2}" | xargs sudo kill -9', (err, stdout, stderr) => {
            if (err) {
            // node couldn't execute the command
            console.log(err);
            console.log('can not do the job!');
                return;
            }

            // the *entire* stdout and stderr (buffered)
            console.log(`stdout: ${stdout}`);
            console.log(`stderr: ${stderr}`);
        });*/
    }

}

function grabNewPosts()
{
    if(grabbing_new_posts)
        return;

    console.log('GRABBING NEW POSTS...');
    console.log('RUNNING_UPDATES_START_TIME:');
    console.log(RUNNING_UPDATES_START_TIME);

    yiic('aggregator/get-posts-to-aggregate 10', (err, stdout, stderr) => {

        grabbing_new_posts = false;

        try {
            var NEW_POSTS_QUEUE = JSON.parse(stdout);

            var data = {};
            POSTS_QUEUE.concat(NEW_POSTS_QUEUE).forEach(function(item) {
                data[item] = true;
            });
            var result = Object.keys(data);

            POSTS_QUEUE = result;
        } catch (e) {
            console.error('JSON received: "' + stdout + '"');
            return console.error(e);
        }

        if(POSTS_QUEUE && POSTS_QUEUE.length) {
            processNewPosts();
        } else {
            console.log('New posts not found. Waiting 5 sec and repeat grabNewPosts');
            setTimeout(grabNewPosts, 5*1000); //5 sec
        }
    });

    grabbing_new_posts = true;
}

grabNewPosts();

