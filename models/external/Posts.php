<?php

namespace app\models\external;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "Posts".
 *
 * @property int $id
 * @property boolean $aggregated_categories
 * @property boolean $aggregated_tags
 * @property boolean $aggregated_posts
 */
class Posts extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%external.posts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['external_id', 'site_id', 'post_url'], 'required'],
            [['title'], 'default', 'value' => ''],
            [['aggregated_categories', 'aggregated_tags', 'aggregated_posts'], 'default', 'value' => false],
            [['img_url', 'post_url'], 'string', 'length' => [1, 255]],
            [['recommend'], 'boolean', 'trueValue' => true, 'falseValue' => false, 'strict' => true],
            [['aggregated_categories', 'aggregated_tags', 'aggregated_posts'], 'boolean', 'trueValue' => true, 'falseValue' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'external_id' => 'External ID',
            'site_id' => 'Site ID',
            'post_url' => 'Post URL',
            'img_url' => 'Img URL',
            'recommend' => 'Recommend an article'
        ];
    }

    public function findByExternalIdAndSiteId($external_id, $site_id)
    {
        return $external_id ? self::find()->where(['external_id' => $external_id, 'site_id' => $site_id])->one() : null;
    }
}
