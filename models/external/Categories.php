<?php


namespace app\models\external;


use yii\db\ActiveRecord;

class Categories extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%external.categories}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['external_id', 'site_id'], 'required'],
            [['name'], 'default', 'value' => '']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'external_id' => 'External ID',
            'site_id' => 'Site ID'
        ];
    }

    public function findByExternalIdAndSiteId($external_id, $site_id)
    {
        return $external_id ? self::find()->where(['external_id' => $external_id, 'site_id' => $site_id])->one() : null;
    }

}
