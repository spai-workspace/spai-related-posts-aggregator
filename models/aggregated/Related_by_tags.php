<?php


namespace app\models\aggregated;


use yii\db\ActiveRecord;

class Related_by_tags extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%aggregated.related_by_tags}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['related_post_id', 'post_id', 'value'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'related_post_id' => 'Related post ID'
        ];
    }

}
