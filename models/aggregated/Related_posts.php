<?php


namespace app\models\aggregated;


use yii\db\ActiveRecord;

class Related_posts extends ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%aggregated.related_posts}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['related_post_id', 'post_id', 'value'], 'required']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'related_post_id' => 'Related post ID'
        ];
    }

}
