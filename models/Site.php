<?php

namespace app\models;

use app\models\aggregated\WidgetImpStat;
use app\models\external\Posts;
use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "site".
 *
 * @property int $id
 * @property string $url
 * @property string $name
 * @property string $category
 * @property int|null $status
 * @property string $api_key
 * @property int $total_posts
 */
class Site extends ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_DELETED = 2;
    /**
     * {@inheritdoc}
     */

    public static function tableName()
    {
        return 'site';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'name', 'api_key'], 'required'],
            [['name','url', 'category'],'filter','filter' => function($value) {return Html::encode($value);}
            ],
            [['status'], 'default', 'value' => 1],
            [['status', 'category', 'total_posts'], 'integer'],
            [['url'], 'string', 'max' => 100],
            [['name', 'api_key'], 'string', 'max' => 50],
            [['url'], 'validateUrl']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => 'Url',
            'name' => 'Name',
            'status' => 'Status',
            'category' => 'Category',
            'api_key' => 'Api Key',
            'total_posts' => 'Total posts'
        ];
    }

    public function generateApiKey()
    {
        $this->api_key = Yii::$app->getSecurity()->generateRandomString(20);
    }

    public function validateUrl()
    {
        if (!filter_var($this->url, FILTER_VALIDATE_URL)) {
            $this->addError('url', 'Incorrect url');
        }
        $url = parse_url($this->url);
        if(isset($url['host'])) {
            $this->url = $url['scheme'].'://'.$url['host'];
        }

    }

    public function findByApiKey($api_key)
    {
        return $api_key ? self::find()->where(['api_key' => $api_key])->one() : null;
    }

    public function getPosts()
    {
        return $this->hasMany(Posts::class, ['site_id' => 'id']);
    }
}
